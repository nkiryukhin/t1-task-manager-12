package ru.t1.nkiryukhin.tm.api;

import ru.t1.nkiryukhin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Integer getSize();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}