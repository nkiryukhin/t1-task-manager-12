package ru.t1.nkiryukhin.tm.api;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void clearProjects();

    void completeProjectById();

    void completeProjectByIndex();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
